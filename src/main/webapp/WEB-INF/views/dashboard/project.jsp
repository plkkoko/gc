<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.ExcessiveAttemptsException"%>
<%@ page import="org.apache.shiro.authc.IncorrectCredentialsException"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<head>
	<title></title>
	<script type="text/javascript">
	$(document).ready(function() {
		//初始化fancyBox
		$("a#openAddProjectBox").fancybox({
		});
	});

	//触发：新增项目PickerBox
	function openAddProjectBox(){
		$("a#openAddProjectBox").trigger('click');
	}
	
	//触发：更新项目PickerBox
	function openUpdateProjectBox(id){
		$("#openAddProjectBox").attr('href','${ctx}/project/update/'+id);
		$("a#openAddProjectBox").trigger('click');
	}
	</script>
	<script type="text/javascript">
	   //ajax加载左侧菜单
		$.ajax({
			url: "${ctx}/ajax/left-menu",
		    cache: true
		}).done(function( html ) {
		   $("#left-menu").html(html);
		});
	</script>	
</head>
</head>

<body>
<a id="openAddProjectBox" data-fancybox-type="iframe" href="${ctx}/project/create"></a>
	
<div class="container-fluid">
	<div class="row-fluid">
		
		<!-- left menu starts -->
		<div class="span2 main-menu-span">
			<div class="well nav-collapse sidebar-nav" id="left-menu">
				
			</div><!--/.well -->
		</div><!--/span-->
		<!-- left menu ends -->
		
		<noscript>
			<div class="alert alert-block span10">
				<h4 class="alert-heading">Warning!</h4>
				<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
			</div>
		</noscript>
		
		<div id="content" class="span10">
		<!-- content starts -->
		

		<div>
			<ul class="breadcrumb">
				<li>
					<a href="${ctx}/dashboard">Home</a> <span class="divider">/</span>
				</li>
				<li>
					<a href="${ctx}/project">Project</a>
				</li>
			</ul>
		</div>
		
		
		<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> 项目</h2>
						<div class="box-icon">
							<a href="javascript:openAddProjectBox();" class="btn btn-primary"><i class="icon-plus"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
							  	  <th>序号</th>
								  <th>项目名称</th>
								  <th>项目描述</th>
								  <th>操作</th>
							  </tr>
						  </thead>   
						  <tbody>
						  <c:forEach items="${projects}" var="project" varStatus="status" >
							<tr>
								<td>${status.count}</td>
								<td>${project.name}</td>
								<td class="center">${project.description}</td>
								<td class="center">
									<a class="btn btn-info" href="javascript:openUpdateProjectBox(${project.id});">
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<a class="btn btn-danger" href='javascript:deleteConfirm("${ctx}/project/delete/${project.id}");'>
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
							</c:forEach>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
			
</div>
</div>
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>确认信息</h3>
		</div>
		<div class="modal-body">
			<p>是否删除？</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">取消</a>
			<a href="" id="confirmYes" class="btn btn-primary">确定</a>
		</div>
	</div>
	<script type="text/javascript">
	function deleteConfirm(data){
		$("#confirmYes").attr('href',data);
		$('#myModal').modal('show');
	}
	
	
	</script>

	
</div><!--/.fluid-container-->

</body>
</html>
