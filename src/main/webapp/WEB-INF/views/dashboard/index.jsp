<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.ExcessiveAttemptsException"%>
<%@ page import="org.apache.shiro.authc.IncorrectCredentialsException"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<head>
	<title></title>
	<script type="text/javascript">
	   //ajax加载左侧菜单
		$.ajax({
			url: "${ctx}/ajax/left-menu",
		    cache: true
		}).done(function( html ) {
		   $("#left-menu").html(html);
		});
	   
		$(function () { 
		    $('#chart-container').highcharts({
		        chart: {
		            type: 'line'
		        },
		        title: {
		            text: '代码生成统计概览'
		        },
		        xAxis: {
		        	categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
		                         'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		        },
		        yAxis: {
		        	 title: {
		                    text: '代码行（loc）'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		        },
		        tooltip: {
	                valueSuffix: 'loc'
	            },
		        series: [{
		            name: '方案一',
		            data: [120, 540, 321,400,1200]
		        }, {
		            name: '方案二',
		            data: [187]
		        }]
		    });
		});
	</script>	
		
</head>
</head>

<body>
<div class="container-fluid">
	<div class="row-fluid">
			
		<!-- left menu starts -->
		<div class="span2 main-menu-span">
			<div class="well nav-collapse sidebar-nav" id="left-menu">
				
			</div><!--/.well -->
		</div><!--/span-->
		<!-- left menu ends -->
		
		<noscript>
			<div class="alert alert-block span10">
				<h4 class="alert-heading">Warning!</h4>
				<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
			</div>
		</noscript>
		
		<div id="content" class="span10">
		<!-- content starts -->
		

		<div>
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a> <span class="divider">/</span>
				</li>
				<li>
					<a href="#">Dashboard</a>
				</li>
			</ul>
		</div>
		
		
		<div class="row-fluid">
			<div class="box span12">
				<div class="box-header well">
					<h2><i class="icon-info-sign"></i> 统计信息</h2>
					<div class="box-icon">
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
					<div id="chart-container" style="width:100%; height:400px;"></div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="box span12">
				<div class="box-header well">
					<h2><i class="icon-info-sign"></i> 文档</h2>
					<div class="box-icon">
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
					<img src="${ctx}/static/images/系统架构.jpg" class="img-polaroid" width="50%">
					<br/><br/>
					<h3>DataMode</h3>
					<jsp:include page="../common/data-mode.jsp"></jsp:include>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- content ends -->
		</div><!--/#content.span10-->
	</div><!--/fluid-row-->
			

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>设置</h3>
		</div>
		<div class="modal-body">
			<p>这是model 窗口的内容</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">关闭</a>
			<a href="#" class="btn btn-primary">保存</a>
		</div>
	</div>

	
</div><!--/.fluid-container-->

</body>
</html>
