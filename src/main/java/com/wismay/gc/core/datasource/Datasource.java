package com.wismay.gc.core.datasource;

import com.wismay.gc.core.metamode.MetaMode;

/**
 * 
 * @author Petter
 *
 */
public interface Datasource {
	
	/**
	 * 获取元数据模型。
	 * @return MetaMode
	 * @throws Exception
	 */
	public MetaMode getMetaMode() throws Exception;
	
	/**
	 * 测试数据源连接，没有异常就是测试连接成功。
	 * @throws Exception
	 */
	public void testConn() throws Exception;

}
