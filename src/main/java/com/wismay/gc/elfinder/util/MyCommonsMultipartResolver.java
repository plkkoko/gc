package com.wismay.gc.elfinder.util;

import javax.servlet.http.HttpServletRequest;

/**
 * 解决 CommonsMultipartResolver与elfinder文件上传冲突问题
 * elfinder处理的request如果被CommonsMultipartResolver处理过，则elfinder提取不到request里面的参数和文件流 <br/>
 * <br/>
 * 经过查看org.springframework.web.multipart.commons.CommonsMultipartResolver的源代码我们发现（我们在spring中使用commons fileupload处理上传），其中有个public boolean isMultipart(HttpServletRequest
 * request)方法，此方法控制着Spring是否对用户的Request进行文件上传处理，于是自定义一个我们自己的CommonsMultipartResolver，继承自org.springframework.web.multipart.commons.CommonsMultipartResolver，并重写其中的public boolean
 * isMultipart(HttpServletRequest request)，在方法中对当前的request进行判断，如果是一个JakartaMultiPartRequest实例，则返回false，告知Spring不需要再对当前的request进行文件上传处理；如果不是，则直接调用父类的isMultipart方法。
 * 
 * @author Peter
 *         2013-9-22
 */
public class MyCommonsMultipartResolver extends org.springframework.web.multipart.commons.CommonsMultipartResolver {

	@Override
	public boolean isMultipart(HttpServletRequest request) {
		if (request.getRequestURI().contains("elfinder")) {
			return false;
		} else {
			return super.isMultipart(request);
		}
	}
}
