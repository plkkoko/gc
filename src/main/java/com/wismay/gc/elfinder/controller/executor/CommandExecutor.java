package com.wismay.gc.elfinder.controller.executor;

public interface CommandExecutor {
	void execute(CommandExecutionContext commandExecutionContext) throws Exception;
}
